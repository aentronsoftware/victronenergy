// Copyright (c) Sandeep Mistry. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
/// New 12.03.2021 Old 11.03.2021
//#include <CAN.h>
#include "ESP32SJA1000.h"
ESP32SJA1000Class CANESP;
#define ON  LOW
#define OFF HIGH
#include <EEPROM.h>

#include "SD.h"
#define SD_MOSI      18
#define SD_MISO      19
#define SD_SCK       5
#define SD_CS_PIN   14

File myFile;
SPIClass SPISD(HSPI);
String semi = ",";
bool checkonce = true;
int counterdatamissing = 0;
int LED_R = 2;
int LED_B = 4;
int LED_G = 15;

int CounterTime = 0;
//// Read data from frames
int Availablenode1 = 0;
int Availablenode2 = 0;
int ModulecommTimeout = 150; //(1 Min)
int ModulecommTimeoutRelay = 24; //(2 Min)
int timeoutVoltBatt1 = 0;
int timeoutVoltBatt2 = 0;
int timeoutTempBatt1 = 0;
int timeoutTempBatt2 = 0;
int timeoudelay = 12; // 12*5(60sec)

byte data181[8];
byte data581[8];
byte data582[8];
byte data182[8];
byte data281[8];
byte data282[8];

byte data305[8];
byte data306[8];

byte data1[8];
byte data2[8];
int dataIds[12];
int idscounter = 0, RelayStatus1Counter = 0, RelayStatus2Counter = 0;

byte Errbites0 = 0b00000000;
byte Errbites1 = 0b00000000;
byte Errbites2 = 0b00000000;
byte Errbites3 = 0b00000000;

double  Battery1Volt = 0, Battery2Volt = 0;

int counter = 0;
//// converted values after reading canframes
int batt1Vol = 0,   batt2Vol = 0;
int batt1Cur = 0,   batt2Cur = 0;
int batt1Soc = 0 ,  batt2Soc = 0;
int batt1Err = 0,   batt2Err = 0;
int batt1Temp = 0 , batt2Temp = 0, batt1Templow = 0, batt2Templow = 0;
int batt1SOH = 0 ,  batt2SOH = 0;
int Batter1NTC1 = 0, Batter1NTC2 = 0, Batter1NTC3 = 0;
int Batter2NTC1 = 0, Batter2NTC2 = 0, Batter2NTC3 = 0;
int Batt1RelaySta = 0, Batt2RelaySta = 0, SMAStatus = 0;
double Batt1CellMax = 0, Batt2CellMax = 0, Batt1CellMin = 0, Batt2CellMin = 0;

int Delaytimer = 0;

double  ScalingFVolt = 0.001;
double  ScalingFCurr = 0.03125;

int TempLimitHighArr = 50;
int TempLimitHighLeav = 45;

int TempLimitLowhArr = -10;
int TempLimitLowhLeav = -5;

int HighVolLimitArr = 59;
int HighVolLimitLeav = 57;

int underVolLimitArr = 42;  //42;
int underVolLimitLeav = 43; //43;

int DeltaVolt = 5;   ///5V 2 min timeout

int CounterWait = 0, CountSend = 0, CounterWaitDelta1 = 0, CounterWaitDelta2 = 0, SDOtimer = 0;

int Timerticks = 0, counterErrorRepeat = 0, longtimeresetRepErr = 0;
bool ActiveDeactiveTimer = false, ActiveDeactiveCounter = false, Errorcount1 = true;
int Battery1CurretErr = 0, Battery1VoltageErr = 0, Battery1UnderVoltErr = 0, Battery1TempErr = 0, Battery2CurretErr = 0, Battery2VoltageErr = 0, Battery2UnderVoltErr = 0, Battery2TempErr = 0;
void Sendreset1();
void Sendreset2();
int ErrorCounter = 5;
int SDayTime, SBatt1SOC, SBatt2SOC, SBatt1Volt, SBatt2Volt, SBatt1Curr, SBatt2Curr, SBatt1HotTemp, SBatt2HotTemp, SBatt1ColdTemp, SBatt2ColdTemp, SBatt1CellMax, SBatt2CellMax, SBatt1CellMin, SBatt2CellMin;
double SBatt1RelaySta, SBatt2RelaySta, SBatt1Error, SBatt2Error, SSMAVolt, SSMACurr, SSMAStatus, SSMATemp;

String Batt = "/Battery";
String EpromN = "0";
String Csv = ".csv";
String Filename ;

/////////////////////////////////////
void setup()
{
  pinMode(LED_R, OUTPUT);
  pinMode(LED_G, OUTPUT);
  pinMode(LED_B, OUTPUT);
  digitalWrite(LED_B, OFF);
  digitalWrite(LED_G, OFF);
  digitalWrite(LED_R, OFF);

  Serial.begin(115200);
  while (!Serial);
  //////////////////////////////////////
  pinMode(SD_CS_PIN, OUTPUT);
  pinMode(SD_CS_PIN, INPUT_PULLUP);
  digitalWrite(SD_CS_PIN, HIGH);

  SPISD.begin(SD_SCK, SD_MISO, SD_MOSI);
  if (!SD.begin(SD_CS_PIN, SPISD))
  {
    //SD_CS_PIN this pin is just the dummy pin since the SD need the input
    Serial.println(F("Card failed, or not present!"));
    digitalWrite(LED_B, ON);
    digitalWrite(LED_G, OFF);
    digitalWrite(LED_R, OFF);
    return; /// Testing 15.06.2021 -> Done
  }
  else
  {
    Serial.println(F("SD is ready to Write!"));
  }
  //////////////////////////////////

  Serial.println("CAN Receiver Callback");
  // register the receive callback
  CANESP.onReceive(onReceive);
  CANESP.setPins(26, 25); /// CANbus
  CANESP.begin(500E3);    /// 500 kbits
  delay(2000);

  EEPROM.begin(2);
  EEPROM.write(1, EEPROM.read(1) + 1);
  EEPROM.commit();
  int check1 =  EEPROM.read(1);
  EpromN = String(check1);
  Filename =  Batt + EpromN + Csv;
  Serial.println(Filename);

  myFile = SD.open(Filename, "a"); //append to file
  String semi = ",";
  String Headline = "5ms Counter" + semi + "Batt1SOC"  + semi + "Batt2SOC"  + semi + "Batt1Volt"  + semi + "Batt2Volt"  + semi + "Batt1Curr"  + semi + "Batt2Curr"  + semi + "Batt1HotTemp"  + semi + "Batt2HotTemp"  + semi + "ABC2SMATemp"  + semi +  "ABC2SMAVolt"   + semi +   "Batt1CellMax" + semi + "Batt2CellMax"  + semi + "Batt1CellMin"  + semi + "Batt2CellMin"  + semi +  "Batt1RelaySta"   + semi + "Batt2RelaySta"   + semi + "Batt1Error"  + semi + "Batt2Error"  + semi + "SMASendVolt"  + semi + "SMASenddCurr"   + semi + "SMASendStatus" + semi + "SMASendTemp" + semi +   "Batter1FNTC1"  + semi + "Batter1MNTC2"  + semi + "Batter1RNTC3" + semi +   "Batter2FNTC1" + semi + "Batter2MNTC2" + semi + "Batter2RNTC3";
  myFile.println(Headline);
  myFile.close();
  Serial.println("Headlines");
}

void loop()
{
  File myFile;

  myFile = SD.open(Filename, "a"); //append to file
  //Serial.print("Sending packet ... ");

  CANESP.beginPacket(0x80);
  CANESP.write(0);
  CANESP.endPacket();
 // Serial.println("done");
  delay(2000);
  ////////////////////////////////////////////
  batt1Soc  = data181[1];
  if (batt1Soc == 0) batt1Soc = 1;
  Serial.print("  BattSOC_1[%]:");
  Serial.print(batt1Soc);

  batt1Err =  data181[2];
  Serial.print("  BattErr_1:");

  if (batt1Err == 0)
  {
    Serial.print("No Error");
    Errbites1 = 0b10000000;
    Errbites0 = 0b10101010;
    Errbites2 = 0b00000000;
    Errbites3 = 0b00000010;
    Battery1CurretErr = 0;
    Battery1VoltageErr  = 0;
    Battery1UnderVoltErr = 0;
    Battery1TempErr = 0;
  }
  if (batt1Err > 0)
  {
    if (batt1Err == 0x02) {
      Serial.print("OVER CURRENT");
      Errbites1 = 0b01000000;
      if (Battery1CurretErr < ErrorCounter)
      {
        Sendreset1();
        Battery1CurretErr++;
      }
    }
    if (batt1Err == 0x04) {
      Serial.print("OVER VOLTAGE");
      Errbites0 = 0b00000100;
      if (Battery1Volt < HighVolLimitLeav)
      {
        if (Battery1VoltageErr < ErrorCounter)
        {
          Sendreset1();
          Battery1VoltageErr++;
        }
      }
    }
    if (batt1Err == 0x08) {
      Serial.print("UNDER VOLTAGE");
      Errbites0 = 0b00010000;
      if (Battery1Volt > underVolLimitLeav)
      {
        if (Battery1UnderVoltErr < ErrorCounter)
        {
          Sendreset1();
          Battery1UnderVoltErr++;
        }
      }
    }
    if (batt1Err == 0x10) {
      Serial.print("DELTA VOLTAGE");
      Errbites3 = 0b00000001;
    }
    if (batt1Err == 0x20) {
      Serial.print("TEMPERATURE");
      Errbites0 = 0b01000000;
      if ((batt1Temp < TempLimitHighLeav) && (batt1Temp > TempLimitLowhLeav))
      {
        if (Battery1TempErr < ErrorCounter)
        {
          Sendreset1();
          Battery1TempErr++;
        }
      }
    }
    if (batt1Err == 0x80)
    {
      Serial.println("COMMUNICATION");
      Sendreset1();
    }
    if (batt1Err == 0xFF) {
      Serial.println("EMV");
      Sendreset1();
    }
  }
  ///////////////////////////////////////////////////
  batt1Vol = (data181[7] << 8) | data181[6];
  Serial.print("  BattVol_1[V]:");
  Battery1Volt = batt1Vol * ScalingFVolt;
  Serial.println(batt1Vol * ScalingFVolt);

  //////////////////////////////////////////////////
  batt2Soc  = data182[1];
  if (batt2Soc == 0) batt2Soc = 1;
  Serial.print("  BattSOC_2[%]:");
  Serial.print(batt2Soc);

  batt2Err =  data182[2];
  Serial.print("  BattErr_2:");
  if (batt2Err == 0)
  {
    if (batt1Err == 0)
    {
      Serial.print("No Error");
      Errbites1 = 0b10000000;
      Errbites0 = 0b10101010;
      Errbites2 = 0b00000000;
      Errbites3 = 0b00000010;
      Battery2CurretErr = 0;
      Battery2VoltageErr = 0;
      Battery2UnderVoltErr = 0;
      Battery2TempErr = 0;
    }
  }
  if (batt2Err == 0x02) {
    Serial.println("OVER CURRENT");
    Errbites1 = 0b01000000;
    if (Battery2CurretErr < ErrorCounter)
    {
      Sendreset2();
      Battery2CurretErr++;
      Serial.print("Battery1CurretErr: ");
      Serial.println(Battery2CurretErr);
    }
  }
  if (batt2Err == 0x04) {
    Serial.print("OVER VOLTAGE");
    Errbites0 = 0b00000100;
    if (Battery2Volt < HighVolLimitLeav)
    {
      if (Battery2VoltageErr < ErrorCounter)
      {
        Sendreset2();
        Battery2VoltageErr++;
      }
    }
  }
  if (batt2Err == 0x08) {
    Serial.print("UNDER VOLTAGE");
    Errbites0 = 0b00010000;
    if (Battery2Volt > underVolLimitLeav)
    {
      if (Battery2UnderVoltErr < ErrorCounter)
      {
        Sendreset2();
        Battery2UnderVoltErr++;
      }
    }
  }
  if (batt2Err == 0x10) {
    Serial.println("DELTA VOLTAGE");
    Errbites3 = 0b00000001;
  }
  if (batt2Err == 0x20) {
    Serial.print("TEMPERATURE");
    Errbites0 = 0b01000000;
    if ((batt2Temp < TempLimitHighLeav) && (batt2Temp > TempLimitLowhLeav))
    {
      if (Battery2TempErr < ErrorCounter)
      {
        Sendreset2();
        Battery2TempErr++;
      }
    }
  }
  if (batt2Err == 0x80) {
    Serial.print("COMMUNICATION");
    Sendreset2();
  }
  if (batt2Err == 0xFF) {
    Serial.print("UNKNWON");
    Sendreset2();
  }
  delay(2000); /// 1000 Not good
  if ((batt2Err > 0) || (batt1Err > 0))
  {
    digitalWrite(LED_G, OFF);
    digitalWrite(LED_G, OFF);

    digitalWrite(LED_R, ON);
    digitalWrite(LED_R, ON);
    delay(100);
  }

  /////////////////////////////////////////////////////
  batt2Vol = (data182[7] << 8) | data182[6];
  Serial.print("  BattVol_2[V]:");
  Battery2Volt = batt2Vol * ScalingFVolt;
  Serial.println(Battery2Volt);

  if ((Battery2Volt > HighVolLimitArr) || (Battery1Volt > HighVolLimitArr))
  {
    timeoutVoltBatt2++;
    if (timeoutVoltBatt2 > timeoudelay)
    {
      Errbites0 = 0b00000100;
    }
  }
  else
  {
    timeoutVoltBatt2 = 0;
  }
  if ((Battery2Volt < underVolLimitArr ) || (Battery1Volt < underVolLimitArr))
  {
    timeoutVoltBatt1++;
    if (timeoutVoltBatt1 > timeoudelay)
    {
      Errbites0 = 0b00010000;
    }
  }
  else
  {
    timeoutVoltBatt1 = 0;
  }

  //////////////////////////////////////////////////
  String MSB1 = String(data181[5], HEX);
  String LSB1 = String(data181[4], HEX);
  signed int batt1Cur = (int16_t)(strtol((MSB1 + LSB1).c_str(), NULL, 16));
  double  Batt1Current = batt1Cur * ScalingFCurr;
  Serial.print("  BattCurr_1[A]:");
  Serial.print(batt1Cur * ScalingFCurr);
  //  Serial.print(" LSB1:");
  //  Serial.print(data181[4], HEX);
  //  Serial.print(" MSB1:");
  //  Serial.print(data181[5], HEX);
  /////////////////////////////////////////////
  String MSB2 = String(data182[5], HEX);
  String LSB2 = String(data182[4], HEX);
  signed int batt2Cur = (int16_t)(strtol((MSB2 + LSB2).c_str(), NULL, 16));
  double  Batt2Current = batt2Cur * ScalingFCurr;
  Serial.print("  BattCurr_2[A]:");
  Serial.print(batt2Cur * ScalingFCurr);
  //  Serial.print(" LSB1:");
  //  Serial.print(data182[4], HEX);
  //  Serial.print(" MSB1:");
  //  Serial.println(data182[5], HEX);
  ///////////////////////////////////////////////
  Serial.print("  BattTemp_1[°C]:");
  batt1Temp = data181[3] ;
  Serial.print(batt1Temp);

  /////////////////////////////////////////////
  batt2Temp = data182[3] ;
  Serial.print("  BattTemp_2[°C]:");
  Serial.println(batt2Temp);

  if ((batt2Temp > TempLimitHighArr) || (batt1Temp > TempLimitHighArr))
  {
    timeoutTempBatt2++;
    if (timeoutTempBatt2 > timeoudelay)
    {
      Errbites0 = 0b01000000;
    }
  }
  else
  {
    timeoutTempBatt2 = 0;
  }
  if ((batt2Temp < TempLimitLowhArr ) || (batt1Temp < TempLimitLowhArr))
  {
    timeoutTempBatt1++;
    if (timeoutTempBatt1 > timeoudelay)
    {
      Errbites0 = 0b01000000;
    }
  }
  else
  {
    timeoutTempBatt1 = 0;
  }

  ////////////////////////////////////////////
  batt1SOH = data181[0] ;
  //  Serial.print("  Battery1SOH :");
  //  Serial.print(batt1SOH);
  /////////////////////////////////////////////
  batt2SOH = data182[0] ;
  //  Serial.print("  Battery2SOH");
  //  Serial.println(batt2SOH);
  ////////////////////////////////////////////
  ////////////////////////////////////////////
  /// SMA Data Reading Every 3Sec
  ////////////////////////////////////////////
  int battVol305 = (data305[1] << 8) | data305[0];
  Serial.print("  battVol305:");
  Serial.print(battVol305 * 0.1);
  SSMAVolt = battVol305 * 0.1;

  String MSB3 = String(data305[2], HEX);
  String LSB3 = String(data305[3], HEX);
  signed int battCurr305 = (int16_t)(strtol((MSB3 + LSB3).c_str(), NULL, 16));
  Serial.print("  battCurr305:");
  SSMACurr  = battCurr305 * 0.1;
  Serial.print(battCurr305 * 0.1);

  int battTemp305 = ((data305[5] << 8) | data305[4]);
  Serial.print("  battTemp305:");
  Serial.print(battTemp305 * 0.1);
  SSMATemp = battTemp305 * 0.1;

  int battSOC305 = (data305[7] << 8) | data305[6];
  Serial.print("  battSOC305:");
  Serial.println(battSOC305 * 0.1);

  ///////////////////////////////////////////
  int battSOH306 = (data306[1] << 8) | data306[0];
  Serial.print("  battSOH306:");
  Serial.print(battSOH306);

  int battChPr306 = data306[2] ;
  Serial.print("  battChProce306:");
  Serial.print(battChPr306);

  int battOpeSta306 = data306[3];
  Serial.print("  battOpeSta306:");
  Serial.print(battOpeSta306);

  int battErrorM306 = (data306[5] << 8) | data306[4];
  Serial.print("  battErrorM306:");
  Serial.print(battErrorM306);
  SMAStatus = battErrorM306;

  int battBattChaVol306 = (data306[7] << 8) | data306[6];
  Serial.print("  battBattChaVol306:");
  Serial.println(battBattChaVol306 * 0.1);

  //////////////////////////////////////////////
  //// When the Batteries have more than 20V and also differnece is greater than 8V Difference
  /// when we remove the 0x181 battery then we see same as old values , but we will repleaser them with 0 values after Read or at then end of Loop better or after 3 counter then Reset to 0
  if ((batt1Err > 0) && (batt2Err == 0))
  {
    Serial.println(CounterWaitDelta1);
    CounterWaitDelta1++;
    if ((CounterWaitDelta1 > 10) && (CounterWaitDelta1 < 20))
    {
      CANESP.beginPacket(0x182);
      CANESP.write(0);
      CANESP.write(0); 
      CANESP.write(10);
      CANESP.write(0); /// Charge Current 1420 -> 0.1
      CANESP.write(0);
      CANESP.write(0); /// Disch Current  3000 -> 0.1
      CANESP.write(0);
      CANESP.write(0); /// Disch Voltage  430 -> 0.1
      CANESP.endPacket();
      delay(500);
    }
  }
  else
  {
    CounterWaitDelta1 = 0;
  }

  if ((batt2Err > 0) && (batt1Err == 0))
  {
    Serial.println(CounterWaitDelta2);
    CounterWaitDelta2++;
    if ((CounterWaitDelta2 > 10) && (CounterWaitDelta2 < 20))
    {
      CANESP.beginPacket(0x181);
      CANESP.write(0);
      CANESP.write(0); 
      CANESP.write(10);
      CANESP.write(0); 
      CANESP.write(0);
      CANESP.write(0); 
      CANESP.write(0);
      CANESP.write(0); 
      CANESP.endPacket();
      delay(500);
    }
  }
  else
  {
    CounterWaitDelta2 = 0;
  }

  /*  if ((Battery1Volt > 25) && (Battery2Volt > 25) && (abs(Battery1Volt - Battery2Volt) > DeltaVolt))
    {
      CounterWaitDelta++;
      Serial.print("CounterWaitDelta:");
      Serial.println(CounterWaitDelta);
      if (CounterWaitDelta > 50) CounterWaitDelta = 0;

      if (CounterWaitDelta > 24) /// 2Min (5Sec increment 1 point)
      {
        /*  Errbites3 = 0b00000001;

          CANESP.beginPacket(0x181);
          CANESP.write(0);
          CANESP.write(0); /// Voltag Charge  450 -> 0.1
          CANESP.write(10);
          CANESP.write(0); /// Charge Current 1420 -> 0.1
          CANESP.write(0);
          CANESP.write(0); /// Disch Current  3000 -> 0.1
          CANESP.write(0);
          CANESP.write(0); /// Disch Voltage  430 -> 0.1
          CANESP.endPacket();
          delay(500);

          CANESP.beginPacket(0x182);
          CANESP.write(0);
          CANESP.write(0); /// Voltag Charge  450 -> 0.1
          CANESP.write(10);
          CANESP.write(0); /// Charge Current 1420 -> 0.1
          CANESP.write(0);
          CANESP.write(0); /// Disch Current  3000 -> 0.1
          CANESP.write(0);
          CANESP.write(0); /// Disch Voltage  430 -> 0.1
          CANESP.endPacket();*/
  //   }
  // }*/

  //////////////////////////////////////////////
  //////////////////////////////////////////////
  /// Average Algorithem
  int SocDivi = 1;
  if ((batt1Soc > 2) && (batt2Soc > 2)) SocDivi = 2;
  int SMASoc = (batt1Soc + batt2Soc) / SocDivi;
  Serial.print("  SMASoc:");
  Serial.print(SMASoc);
  ///////////////////////////////////////////
  int SoHDivi = 1;
  if ((batt1SOH > 2) && (batt2SOH > 2)) SoHDivi = 2;
  int SMASOH = (batt1SOH + batt2SOH) / SoHDivi;
  Serial.print("  SMASOH:");
  Serial.print(SMASOH);
  ///////////////////////////////////////////
  int VoltDivi = 1;
  if ((Battery2Volt > 20) && (Battery1Volt > 20)) VoltDivi = 2;
  int SMABattVoltage = ((Battery1Volt + Battery2Volt) / VoltDivi) * 100;
  Serial.print("  SMABattVoltage:");
  Serial.print(SMABattVoltage);

  //  Serial.print("SMABattVoltage HEX:");
  //  Serial.print((SMABattVoltage) & 0xFF, HEX);
  //  Serial.println((SMABattVoltage) >> 8, HEX);
  ///////////////////////////////////////////
  int CurrentDivi = 2;
  //if ((Battery1Volt > 20) && (Battery1Volt > 20)) VoltDivi = 2;
  int SMABattcurrent = ((Batt1Current + Batt2Current) / VoltDivi) * 10;
  Serial.print("  SMABattcurrent:");
  Serial.print(SMABattcurrent);

  //  Serial.print("SMABattcurrent HEX:");
  //  Serial.print((SMABattcurrent) & 0xFF, HEX);
  //  Serial.println((SMABattcurrent) >> 8, HEX);
  ///////////////////////////////////////////
  int TempDivi = 2;
  // if ((Battery1Volt > 20) && (Battery1Volt > 20)) VoltDivi = 2;
  int SMABattTemp = ((batt1Temp + batt2Temp) / VoltDivi) * 10;
  Serial.print("  SMABattTemp:");
  Serial.println(SMABattTemp);
  //  Serial.print("SMABattTemp HEX:");
  //  Serial.print((SMABattTemp) & 0xFF, HEX);
  //  Serial.println((SMABattTemp) >> 8, HEX);
  ///////////////////////////////////////////
  for (int i = 0; i < 12; i++)
  {
    //    Serial.print("IDS: ");
    //    Serial.println(dataIds[i], HEX);

    if ((dataIds[i] == 385) || (dataIds[i] == 641) || (dataIds[i] == 897) || (dataIds[i] == 1153))
    {
      Availablenode1 = 0;
    }
    else
    {
      Availablenode1++;
    }
    if ((dataIds[i] == 386) || (dataIds[i] == 642) || (dataIds[i] == 898) || (dataIds[i] == 1154))
    {
      Availablenode2 = 0;
    }
    else
    {
      Availablenode2++;
    }
    dataIds[i] = 0;
  }
  //  Serial.print("  MMS Avaiable Node1: ");
  //  Serial.print(Availablenode1);
  //  Serial.print("  MMS Avaiable Node2: ");
  //  Serial.println(Availablenode2);
  if (Availablenode1 > ModulecommTimeout)  Errbites0 = 0b00000001;
  if (Availablenode2 > ModulecommTimeout)  Errbites0 = 0b00000001;
  //////////////////////////////////////////
  /////////////////////////////////////////
  ////////////////////////////////
  /// Relay Status and Software Version
  ///////////////////////////////
  SDOtimer++;
  if (SDOtimer > 7234) SDOtimer = 0;

  if ((SDOtimer == 1) || (SDOtimer > 7200)) /// 7200 2 Hours
  {
    CANESP.beginPacket(0x601);
    CANESP.write(0x40);
    CANESP.write(0x00);
    CANESP.write(0x61);
    CANESP.write(0x01);
    CANESP.write(0x00);
    CANESP.write(0x00);
    CANESP.write(0x00);
    CANESP.write(0x00);
    CANESP.endPacket();
    ///////////////////////////////
    CANESP.beginPacket(0x602);
    CANESP.write(0x40);
    CANESP.write(0x00);
    CANESP.write(0x61);
    CANESP.write(0x01);
    CANESP.write(0x00);
    CANESP.write(0x00);
    CANESP.write(0x00);
    CANESP.write(0x00);
    CANESP.endPacket();
  }
  //////////////////////////////////////////
  //  Serial.print("RelayStatus1Counter: ");
  //  Serial.println(RelayStatus1Counter);
  //  Serial.print("RelayStatus2Counter: ");
  //  Serial.println(RelayStatus2Counter);
  if (RelayStatus1Counter > ModulecommTimeoutRelay)  Errbites0 = 0b00000001;
  if (RelayStatus2Counter > ModulecommTimeoutRelay)  Errbites0 = 0b00000001;
  //////////////////////////////////////////
  //////////////////////// Settings SMA
  //// if SMA dont receice 0x351  then SMS ends waarning W952 WrnExtBMSTmOut
  CANESP.beginPacket(0x351);
  CANESP.write(0x3A);
  CANESP.write(0X02); /// Voltag Charge  570   -> 0.1 (Offset -1V)
  CANESP.write(0x40);
  CANESP.write(0x06); /// Charge Current 1600 -> 0.1
  CANESP.write(0x40);
  CANESP.write(0x06); /// Disch Current  1600 -> 0.1
  CANESP.write(0x9A);
  CANESP.write(0x01); /// Disch Voltage  410   -> 0.1
  CANESP.endPacket();
  delay(200);
  ///////////////////////////
  //// if SMA dont receice 0x355  thne SMA ends error F952 ExtBMSTimeout
  CANESP.beginPacket(0x355);
  CANESP.write(SMASoc);
  CANESP.write(0x00);     /// SOC
  CANESP.write(SMASOH);
  CANESP.write(0x00);     /// SOH
  CANESP.write(0X00);
  CANESP.write(0x00);     /// HiResSOC -> 0.01
  CANESP.write(0X00);
  CANESP.write(0x00);
  CANESP.write(0x00);
  CANESP.endPacket();
  delay(200);
  ///////////////////////////////////
  ///////////////////////////////////   /// aktulle Batter data
  CANESP.beginPacket(0x356);
  CANESP.write(SMABattVoltage & 0xFF);
  CANESP.write(SMABattVoltage >> 8);    /// Battery Voltage -> 0.01 ->5600
  CANESP.write(SMABattcurrent & 0xFF);
  CANESP.write(SMABattcurrent >> 8);    /// Battery Current -> 0.1
  CANESP.write(SMABattTemp & 0xFF);
  CANESP.write(SMABattTemp >> 8);       /// Battery Temperature -> 0.1
  CANESP.write(0X00);
  CANESP.write(0x00);
  CANESP.endPacket();
  delay(200);
  ///////////////////////////////////
  ///////////////////////////////////   /// aktulle Batter data
  //  byte x = 0b10000000;  // the 0b prefix indicates a binary constant
  //  Serial.println(Errbites, BIN); // 10000000

  //  Serial.print("Errbites0: ");
  //  Serial.println(Errbites0, BIN);
  //  Serial.print("Errbites1: ");
  //  Serial.println(Errbites1, BIN);
  //  Serial.print("Errbites2: ");
  //  Serial.println(Errbites2, BIN);
  //  Serial.print("Errbites3: ");
  //  Serial.println(Errbites3, BIN);

  CANESP.beginPacket(0x35A);
  CANESP.write(Errbites0);    /// Battery Alarm0
  CANESP.write(Errbites1);    /// Battery Alarm1
  CANESP.write(Errbites2);    /// Battery Alarm2
  CANESP.write(Errbites3);    /// Battery Alarm3
  CANESP.write(0x00);
  CANESP.write(0x00);         /// Battery Warnungs
  CANESP.write(0X00);
  CANESP.write(0x00);
  CANESP.endPacket();
  delay(200);
  ///////////////////////////////////
  /// Manufacturer-Name-ASCII AENBATT
  //////////////////////////////////
  CANESP.beginPacket(0x35E);
  CANESP.write(0x41);
  CANESP.write(0x45);    /// AEN
  CANESP.write(0x4e);
  CANESP.write(0x42);    /// BAT
  CANESP.write(0x41);
  CANESP.write(0x54);
  CANESP.write(0X00);
  CANESP.write(0x00);
  CANESP.endPacket();
  delay(200);
  ///////////////////////////////////
  /// Manufacturer-Name-ASCII AENBATT
  //////////////////////////////////
  CANESP.beginPacket(0x35F);
  CANESP.write(0x18);
  CANESP.write(0x00);    /// Battery Type 48V
  CANESP.write(0x34);
  CANESP.write(0x00);    /// Battery BMS Version V52
  CANESP.write(0x96);
  CANESP.write(0x01);    /// Battery Capacity 406Ah
  CANESP.write(0X72);
  CANESP.write(0x0A);    /// MANUFATURE ID
  CANESP.endPacket();

  ///////////////////////////////////
  /// Emergency shutdown
  //////////////////////////////////
  CANESP.beginPacket(0x35B);
  CANESP.write(0b00000000);
  CANESP.write(0b00000000);
  CANESP.write(0b00000000);
  CANESP.write(0b00000000);
  CANESP.write(0b00000000);
  CANESP.write(0b00000000);
  CANESP.write(0b00000000);
  CANESP.write(0b00000000);
  CANESP.endPacket();

  Batt1CellMax  = data281[0] * 0.02;
  Batt1CellMin  = data281[1] * 0.02;
  Batt1RelaySta = data281[2];
  Batter1NTC1 = data281[3];
  Batter1NTC2 = data281[4];
  Batter1NTC3 = data281[5];

  Batt2CellMax  =  data282[0] * 0.02;
  Batt2CellMin  = data282[1] * 0.02;
  Batt2RelaySta = data282[2];
  Batter2NTC1 = data282[3];
  Batter2NTC2 = data282[4];
  Batter2NTC3 = data282[5];

  ///////////////////////////////////////////////////////////
  /*
  */
  if (myFile)
  {
    CounterTime++;
    String Headline1 = String(CounterTime)  + semi + String(batt1Soc)  + semi + String(batt2Soc)  + semi + String(Battery1Volt, 3)  + semi + String(Battery2Volt, 3)  + semi + String(Batt1Current, 3)  + semi + String(Batt2Current, 3)  +
                       semi + String(batt1Temp)  + semi + String(batt2Temp)  + semi + String(SMABattTemp * 0.1, 3)  + semi +  String(SMABattVoltage * 0.01, 3)   + semi +   String(Batt1CellMax, 3) + semi + String(Batt2CellMax, 3)  + semi + String(Batt1CellMin, 3)  +
                       semi + String(Batt2CellMin, 3)  + semi +  String(Batt1RelaySta)  + semi + String(Batt2RelaySta) + semi + String(batt1Err) + semi + String(batt2Err)  + semi + String(SSMAVolt, 3) + semi + String(SSMACurr, 3)   + semi + String(SMAStatus) +
                       semi + String(SSMATemp) + semi + String(Batter1NTC1) + semi + String(Batter1NTC2) + semi + String(Batter1NTC3) + semi + String(Batter2NTC1) + semi + String(Batter2NTC2) + semi + String(Batter2NTC3);

    myFile.println(Headline1);
    myFile.close();
    Serial.print("Writing to Battery.csv...");
    //    digitalWrite(LED_B, ON);
    //    digitalWrite(LED_G, OFF);
    //    digitalWrite(LED_R, OFF);
  }
  else
  {
    Serial.println("error opening Battery.csv to write");

    digitalWrite(LED_B, ON);
    digitalWrite(LED_G, OFF);
    digitalWrite(LED_R, OFF);
    return;
  }
  ////////////////////////////////////////////////////////
}

void onReceive(int packet)
{
  //  Serial.print("packet with id 0x");
  //  Serial.println(CANESP.packetId(), HEX);

  dataIds[idscounter] = CANESP.packetId();
  idscounter++;
  if (idscounter > 12) idscounter = 0;

  for (int i = 0; i < 8; i++)
  {
    if (CANESP.available())
    {
      counterdatamissing = 0;
      if (CANESP.packetId() == 0x181)
      {
        data181[i] = CANESP.read();
        digitalWrite(LED_G, ON);
        digitalWrite(LED_R, OFF);
      }
      else if (CANESP.packetId() == 0x281)
      {
        data281[i] = CANESP.read();
        digitalWrite(LED_G, ON);
        digitalWrite(LED_R, OFF);
      }
      else if (CANESP.packetId() == 0x282)
      {
        data282[i] = CANESP.read();
        digitalWrite(LED_G, ON);
        digitalWrite(LED_R, OFF);
      }
      else if (CANESP.packetId() == 0x581)
      {
        data581[i] = CANESP.read();
        if (i == 4)
        {
          if (data581[i] != 0)
          {
            RelayStatus1Counter = 0;
          }
          else
          {
            RelayStatus1Counter++;
          }
        }
      }
      else if (CANESP.packetId() == 0x582)
      {
        data582[i] = CANESP.read();
        if (i == 4)
        {
          if (data582[i] != 0)
          {
            RelayStatus2Counter = 0;
          }
          else
          {
            RelayStatus2Counter++;
          }
        }
      }
      else if (CANESP.packetId() == 0x182)
      {
        data182[i] = CANESP.read();
        digitalWrite(LED_G, ON);
        digitalWrite(LED_R, OFF);
      }
      else if (CANESP.packetId() == 0x305)
      {
        data305[i] = CANESP.read();
        digitalWrite(LED_G, ON);
        digitalWrite(LED_R, OFF);
      }
      else if (CANESP.packetId() == 0x306)
      {
        data306[i] = CANESP.read();
        digitalWrite(LED_G, ON);
        digitalWrite(LED_R, OFF);
      }
      else
      {
        digitalWrite(LED_R, ON);
        digitalWrite(LED_G, OFF);
      }
    }
    else
    {
      //Serial.println("data Received times Max 20");
     // Serial.print(counterdatamissing);
      counterdatamissing++;
      if (counterdatamissing > 20)
      {
        Serial.println("No data Received");
        digitalWrite(LED_R, ON);
        digitalWrite(LED_G, OFF);
      }
    }
  }
}
void Sendreset1()
{
  for (int i = 0; i < 5; i++)
  {
    CANESP.beginPacket(0x81);
    CANESP.write(3);
    CANESP.endPacket();
    Serial.println("Reset");
    delay(300);
  }
}
void Sendreset2()
{
  for (int i = 0; i < 5; i++)
  {
    CANESP.beginPacket(0x81);
    CANESP.write(4);
    CANESP.endPacket();
    Serial.println("Reset");
    delay(300);
  }
}

// ID=305,Type=D,Length=8,Data=EF 01 FF FA DC 00 E8 03,CycleTime=5000,Paused=1,IDFormat=hex
